import { Route, RouteProps, useNavigate, Routes } from "react-router-dom";
import { Login } from "../pages/Account/Login";
import React from "react";
import { AccountState } from "../store/account/types";
import { useSelector } from "react-redux";
import { AppState } from "../store";

export const AccountRoute = ({
  children,
  ...rest
}: RouteProps): JSX.Element => {
  const account: AccountState = useSelector((state: AppState) => state.account);

  console.log("data", account)
  const navigate = useNavigate();

  if (account.token) {
    navigate("/admin/home");
    return <div>error</div>;
  }

  return (
    <Routes>
      <Route {...rest} path="/" element={<Login />} />
    </Routes>
  );
};
