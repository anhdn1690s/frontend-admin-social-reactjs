import { AccountState, LOGIN_ERROR, LOGIN_OUT, LOGIN_REQUEST, LOGIN_SUCCESS, AccountActionTypes } from './types';

const initialState: AccountState = {
    user:null,
    loading:false,
    error: null,
    token: null,
}

const accountReducer = (
    state: AccountState  = initialState,
    action: AccountActionTypes
): AccountState => {
    switch(action.type) {
        case LOGIN_REQUEST: {
            return{
                ...state,
                loading: true
            };
        }
        case LOGIN_SUCCESS: {
            return{
                ...state, 
                loading: false, 
                token:action.payload.token
            }
        }
        case LOGIN_ERROR: {
            return{
                ...state, 
                loading:false, 
                token:null, 
                error:action.payload.error
            }
        }
        case LOGIN_OUT: {
            return{
                ...state, 
                user:null, 
                token:null, 
                error:null
            }
        }
        default:
            return state;
    }
}
export { accountReducer }