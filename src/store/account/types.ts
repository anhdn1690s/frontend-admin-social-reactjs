export const LOGIN_REQUEST = 'LOGIN_REQUEST'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGIN_ERROR = 'LOGIN_ERROR'

export const LOGIN_OUT = 'LOGIN_OUT'

export interface AuthenticatedUser {
    _id:string;
    first_name: string;
    last_name: string;
    email: string;
    avatar: string;
}

export interface LoginRequest {
    type: typeof LOGIN_REQUEST;
    payload: {
        email:string;
        password:string;
    }
}

export interface LoginSuccess {
    type: typeof LOGIN_SUCCESS;
    payload: {
        token:string
    }
}

export interface LoginError {
    type: typeof LOGIN_ERROR;
    payload: {
        error:string
    }
}

export interface Logout {
    type: typeof LOGIN_OUT;
}

//luu du lieu trong store
export interface AccountState {
    user: AuthenticatedUser | null;
    loading: boolean;
    error: string | null;
    token:string | null;
}

export type AccountActionTypes = 
    | LoginRequest
    | LoginSuccess
    | LoginError
    | Logout