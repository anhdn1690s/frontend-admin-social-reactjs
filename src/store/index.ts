import { applyMiddleware, combineReducers, createStore } from "redux";
import  thunkMiddleware  from "redux-thunk";
import { accountReducer } from "./account/reducers";

const rootReducer = combineReducers({
    account: accountReducer
})

export type AppState = ReturnType<typeof rootReducer>;

export default function configureStore()  {
    const middleware = [thunkMiddleware];
    const middlewareEnhancer = applyMiddleware(...middleware);
    return createStore(rootReducer, middlewareEnhancer);
}