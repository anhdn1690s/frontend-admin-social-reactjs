import "./App.css";
import "./styles/sb-admin-2.min.css";

import React from "react";
import {  BrowserRouter as Router } from "react-router-dom";

import { Admin } from "./pages/Admin";
import { Login } from "./pages/Account";
import { PrivateRoute } from "./components/PrivateRoute";
import { AccountRoute } from "./components/AccountRoute";

function App() {
  return (
    <div className="App" id="wrapper">
      <Router>
        <AccountRoute>
          <Login />
        </AccountRoute>
        <PrivateRoute>
          <Admin />
        </PrivateRoute>
      </Router>
    </div>
  );
}

export default App;
